﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cloudIMusic
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.vkApp = VKApi.VKApp.Instance;
            this.vkApp.AppId = "3060820";
            this.vkApp.IsCrossDomain = true;
            this.vkApp.User = new VKApi.VKUser() { AccessToken = "6b66cd4561d5b97361d5b973ea61fb0d27661d561d4b97b317575fadf25de1c2fd2ae1d" };

            // Test
            this.playlist = new List<PlaylistMusic>();
            this.playlist.Add(new PlaylistMusic() { MusicFullName = "No music", Position = 1, URL = "" });
            //this.playlistListView.DataSource = this.playlist;
            //this.playlistListView.DataBind();
        }

        protected VKApi.VKApp vkApp;
        protected List<PlaylistMusic> playlist;
        
        protected void searchButton_Click(object sender, EventArgs e)
        {
            ScriptReference vkAudioSearchScript = null;

            // Warning! Exception! 
            try
            {
                vkAudioSearchScript = this.ScriptManager1.Scripts.Single<ScriptReference>
                    (v => v.Path.Substring(0, 38) == VKApi.VK.AudioSearchUrl);
            }
            catch (System.InvalidOperationException ex)
            {
                // TODO: Log this exception
            }

            if (vkAudioSearchScript == null)
            {
                this.vkApp.Request = new VKApi.VKRequest.Request("audio.search", true, "vkAudioSearchCallback");
                this.vkApp.Request.Params = new List<KeyValuePair<string, string>>() {
                    new KeyValuePair<string, string>("v", "2.0"),
                    new KeyValuePair<string, string>("q", this.searchTextInput.Value),
                    new KeyValuePair<string, string>("count", "20"),
                    new KeyValuePair<string, string>("offset", "1")
                };

                this.ScriptManager1.Scripts.Add(new ScriptReference(vkApp.GetRequestUri().ToString()));
            }
            
            //this.playlist.Clear();
            //this.playlist.AddRange(VKApi.VKMethods.AudioGet(this.SearchText.Value, count: "20"));
            //this.playlistListView.DataBind();
        }
    }
}
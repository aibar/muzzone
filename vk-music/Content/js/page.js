﻿$(document).ready(function () {

    $(function () {
        $("#side-bar").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
        $("#side-bar li").removeClass("ui-corner-top").addClass("ui-corner-left");
    });



    var tabHide;
    //var scroll = $('#now-playing').position().top - window.innerHeight + 130;
    //alert(scroll);

    //$('html').scrollTop(scroll);
    //window.scrollTo(0, scroll);

    $('#top-bar .search-input').focus(function () {
        if (this.value == this.defaultValue) this.value = '';
        $(this).parent().children('.logo').children('.cursor').html('>');
        $('#top-bar .block-full').css('background', 'url(/Content/images/bottom-back.png)');
        $('#top-bar .logo').css('background', 'url(/Content/images/logo-cmd.png)');
        $('#main-bg .block-left').addClass('block-active');
    });
    $('#top-bar .search-input').blur(function () {
        if (this.value == '') this.value = this.defaultValue;
        $(this).parent().children('.logo').children('.cursor').html('|');
        $('#top-bar .block-full').css('background', 'url(/Content/images/top-back.png)');
        $('#top-bar .logo').css('background', 'url(/Content/images/logo.png)');
        $('#main-bg .block-left').removeClass('block-active');
    });

    $('.side-tab .tab-content').mouseout(function () {
        tabHide = window.setTimeout(function () { $("#side-bar").tabs("option", "active", 0); }, 5000);
    });
    $('.side-tab .tab-content').mouseover(function () {
        window.clearTimeout(tabHide);
    });


    $('#side-bar .side-nav li').click(function () {
        $('#main-bg .block-right').addClass('block-active');
    });
});
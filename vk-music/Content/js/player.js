﻿$(document).ready(function () {

    soundManager.setup({
        url: '/Content/swf/sm2_swf-files/',
        flashVersion: 9, // optional: shiny features (default = 8)
        useFlashBlock: false, // optionally, enable when you're ready to dive in
        /**
        * read up on HTML5 audio support, if you're feeling adventurous.
        * iPad/iPhone and devices without flash installed will always attempt to use it.
        */
        onready: function () {
            soundManager.setVolume('music', 50);
            // Ready to use; soundManager.createSound() etc. can now be called.
        }
    });

    function playMusic(item) {
        alert($(item).children('.play').children('a').attr('href'));
    }

    function setVolume(toValue) {
        soundManager.setVolume('music', toValue);
    }

    $('#controls .volume-ctrl .volume').click(function (e) {
        alert();
        console.log(e.pageX);
    });

    function setPosition(toValue) {
        soundManager.setVolume('music', toValue);
        for (i = 1; i <= toValue / 2; i++) {
            $('#volume-and-track .volume .' + i).css('color', '#db1a1a');
        }
    }

    $('#volume-and-track .track span').click(function () {
        music = soundManager.setPosition('music', 200);
    });

    lastItem = null;
    jquery_ui_slider_enable($('#bottom-length-slider'), $('#bottom-length-slider').parent().children('.tooltip'), setPosition, 0, 0);
    jquery_ui_slider_enable($('#bottom-volume-slider'), $('#bottom-volume-slider').parent().children('.tooltip'), setVolume, 50, 100);

    function jquery_ui_slider_enable(slider, tooltip, slideFunc, initValue, max) {
        tooltip.hide();
        slider.slider({
            animate: 'fast',
            range: "min",
            max: max,
            min: 0,
            value: initValue,
            start: function (event, ui) {
                tooltip.fadeIn('fast');
            },
            slide: function (event, ui) {
                tooltip.css('left', ui.value - 12).text(ui.value);
                slideFunc(ui.value);
            },
            stop: function (event, ui) {
                tooltip.fadeOut('fast');
            }
        });
    }



    // Playlist
    mymusic = null;
    prevItem = null;

    $('.playlist').delegate('.item', 'click', function () {
        playMusic(this);
    });

    function playMusic(item) {
        var url = $(item).children('.music').children('.options').children('.download-btn').children('a').attr('href');
        var duration = $(item).children('.duration').attr('value'); //alert(duration);
        soundManager.stop('music');
        soundManager.createSound({
            id: 'music',
            url: url,
            usePeakData: false,
            useWaveformData: false,
            useEQData: true,
            onstop: function () {
                this.destruct();
            },
            onfinish: function () {
                this.stop();
                playMusic($(item).next('.item'));
            },
            whileplaying: function () {
                console.log('spec value = ' + this.eqData);
            }
        });
        soundManager.play('music');
        //window.setTimeout(function () {
        //  var thi = soundManager.getSoundById('music');
        ////var spectrumValue = (+thi.eqData.left[1] + +thi.eqData.right[1]) / 2.0;
        //console.log('spec value = ' + thi.eqData);
        //}, 1000 / 60);
        mymusic = $(item);

        var singer = $(item).children('.music').children('.title').children('.singer').html();
        var song = $(item).children('.music').children('.title').children('.song').html();
        var title = singer + song;

        if (title.length > 55) {
            song = song.substring(0, song.length - (title.length - 55)) + '...';
        }

        $('#top-bar .search-input').val(singer + ' - ' + song);

        $(item).addClass('now-playing');
        //alert(url);
        prevItem = item;
    }



    $('#controls .next-btn').click(function () {
        mymusic.removeClass('now-playing');
        mymusic = mymusic.next('.item');
        playMusic(mymusic);
        mymusic.addClass('now-playing');
    });

    $('#controls .prev-btn').click(function () {
        mymusic.removeClass('now-playing');
        mymusic = mymusic.prev('.item');
        playMusic(mymusic);
        mymusic.addClass('now-playing');
    });

    $('#controls .play-btn').click(function () {
        if (!$(this).hasClass('paused')) {
            soundManager.pause('music');
            $(this).addClass('paused');
        }
        else {
            soundManager.play('music');
            $(this).removeClass('paused');
        }
    });

});

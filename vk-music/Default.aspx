﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="cloudIMusic.Default" %>

<!DOCTYPE html>
<html>

<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="Content/css/style.css" rel="stylesheet" type="text/css" />
    <title>vk-music finder</title>
</head>

<script type="text/javascript">
    function vkAudioSearchCallback(result) {
        var data = $.parseJSON(JSON.stringify(result));

        $('#playlist').html('');
        for (var i = 1, len = data.response.length; i < len; i++) {
            console.log(data.response[i]);
            var title = data.response[i].artist + data.response[i].title;
            var song = data.response[i].title;
            if (title.length > 70) {
                song = song.substring(0, song.length - (title.length - 70)) + '...';
            }

            var item = '<div class="item" onclick="playMusic(this);"><div class="title">'
                            + '<span class="singer">' + data.response[i].artist + '</span> - '
                            + '<span class="song">' + song + '</span>'
                            + '</div><div class="options visible-options"><div class="add"></div></div><div class="options hidden-options"><span class="download"><a href="' + data.response[i].url + '">скачать</a></span></div>'
                            + '<div class="expanded"><div class="length-ctrl"><div class="length track"><div class="position"></div></div></div><span style="margin-left:4px;">2:12/5:23</span><div class="options"><span>добавить</span></div></div>'
                            + '<input type="hidden" class="duration" value="' + data.response[i].duration + '" />';
            $('#playlist').append(item);
            $('#playlist .item').each(function () {
                $(this).bind({
                    mouseenter: function (e) {
                        $(this).children('.hidden-options').fadeIn(0);
                        //$(this).prev('.item').css('border-bottom', '1px solid #000');
                    },
                    mouseleave: function (e) {
                        $(this).children('.hidden-options').fadeOut(0);
                        //$(this).prev('.item').css('border-bottom', '1px dotted #000');
                    }
                });
            });
        }
        console.log("end json");
    }
</script>

<body>
    <input type="hidden" id="audioJson" runat="server" />
    <form id="form1" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="SearchButton" />
        </Triggers>
    </asp:UpdatePanel>

    <div id="top-menu">
    <div class="wrapper">
        <div class="top-menu">
            <div class="emblem"></div>
            <div>главная</div>
            <div>о проекте</div>
        </div>
    </div>
    </div>
    
    <div id="header">
    <div class="wrapper">
        <span id="site-name">vk-music.</span>
        <span id="now-playing-music-title">
            <span class="singer">3 doors down</span> <span class="song">Let me be myself</span>
        </span>
    </div>
    </div>
    
    <div id="controls">
    <div class="wrapper">
        <div class="volume-ctrl">
            <div class="volume track"><div class="position"></div></div>
        </div>
        <div class="length-ctrl">
            <div class="length track"><div class="position"></div></div>
        </div>
        <div class="ctrl-btns">
            <div class="prev-btn"></div>
            <div class="next-btn"></div>
            <div class="play-btn"></div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
        var volume = "<span class='0'>mute</span>",
        track = "<span class='0'>again</span>";
        for (i = 1; i <= 50; i++) {
            volume += "<span class='" + i + "'>-</span>";
        }
        volume += "<span class='50'>max</span>";
        $('#volume-and-track .volume').html(volume);
        for (i = 1; i <= 118; i++) {
            track += "<span class='" + i + "'>-</span>";
        }
        track += "<span class='go-go'>go-go</span>";
        $('#volume-and-track .track').html(track);
    </script>
    
    <div id="search">
    <div class="wrapper">
<%--        <div class="level-1 n-text">
            <div class="left">я хочу слушать</div>
            <div class="right">
                <span>искать в:</span>
                <span id="searchFromGlobal" class="switcher-active">вконтакте</span>
                <span id="searchFromPlaylist" class="switcher">плейлисты</span>
            </div>
        </div>--%>
        <div class="level-2 n-text">
            <div class="search-input-container">
                <input type="text" id="searchTextInput" class="search-input" value="green day" runat="server" />
                <asp:Button runat="server" ID="searchButton" CssClass="search-button" OnClick="searchButton_Click" />
            </div>
        </div>
<%--        <div class="level-3 n-text">
            last.fm says:  3 doors down green day 30 seconds to mars
        </div>--%>
    </div>
    </div>
   
    <div id="content">
    <div class="wrapper">
        <div class="left">
            <%--<p class="content-title n-text">Результат поиска</p>--%>
            <div id="playlist" class="playlist">
                <div class="item" style="background:#000;">
                    <div class="title">
                        <span class="singer n-text" style="color:#fff; text-transform:none;">Результат поиска</span>
                    </div>
                </div>
                <div class="item" onclick="playMusic(this);">
                    <div class="title">
                        <span class="singer">3 Doors Down</span> - <span class="song">American Idiot</span>
                    </div>
                    <div class="options visible-options">
                        <div class="add btn"></div>
                    </div>
                    <div class="options hidden-options">
                        <span class="download"><a href="f73f6de74c0c.mp3">скачать</a></span>
                    </div>
                    
                    <div class="expanded">
                        <div class="length-ctrl">
                            <div class="length track"><div class="position"></div></div> 
                        </div><span style="margin-left:4px;">2:12/5:23</span>
                        <div class="options">
                            <span>добавить</span>
                        </div>
                    </div>
                    <input type="hidden" class="duration" value="174">
                </div>
                <div class="item" onclick="playMusic(this);">
                    <div class="title">
                        <span class="singer">Green Day</span> - <span class="song">American Idiot</span>
                    </div>
                    <div class="options visible-options">
                        <div class="add btn"></div>
                    </div>
                    <div class="options hidden-options">
                        <span class="download"><a href="http://cs4513.userapi.com/u52753499/audios/b9ab3591d680.mp3">скачать</a></span>
                    </div>
                    
                    <div class="expanded">
                        <div class="length-ctrl">
                            <div class="length track"><div class="position"></div></div>
                        </div>
                        <div class="options">
                            <span>добавить</span>
                        </div>
                    </div>
                    <input type="hidden" class="duration" value="174">
                </div>

            </div>
        </div>
        
        <div class="right">
            <p class="content-title n-text">Мои плейлисты</p>
            
            <div id="my-playlists" class="playlist">
                <div class="item">
                    <div class="title"><a href="">Это - в разработке</a></div>
                    <div class="options visible-options"><div class="delete"></div></div>
                </div>
                <div class="item">
                    <div class="title"><a href="">Это - в разработке</a></div>
                    <div class="options visible-options"><div class="delete"></div></div>
                </div>
                <div class="item">
                    <div class="title"><a href="">Это - в разработке</a></div>
                    <div class="options visible-options"><div class="delete"></div></div>
                </div>
                <div class="item">
                    <div class="title"><a href="">Это - в разработке</a></div>
                    <div class="options visible-options"><div class="delete"></div></div>
                </div>
                <div class="item">
                    <div class="title"><a href="">Это - в разработке</a></div>
                    <div class="options visible-options"><div class="delete"></div></div>
                </div>
                <div class="item">
                    <div class="title"><a href="">Это - в разработке</a></div>
                    <div class="options visible-options"><div class="delete"></div></div>
                </div>
                <div class="item">
                    <div class="title"><a href="">Это - в разработке</a></div>
                    <div class="options visible-options"><div class="delete"></div></div>
                </div>
                <div class="item">
                    <div class="title"><a href="">Это - в разработке</a></div>
                    <div class="options visible-options"><div class="delete"></div></div>
                </div>

            </div>
            <div class="clear-float"></div>
        </div>
    </div>
    </div>
    
    <div id="footer" class="notation-text">
    <div class="wrapper">
        Создан супер героем который, в супер битве одалел супер лень
    </div>
    </div>

    </form>

    <script src="Content/js/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="Content/js/soundmanager2-jsmin.js" type="text/javascript"></script>
    <script src="Content/js/player.js" type="text/javascript"></script>
    <script src="Content/js/div_scroll.js" type="text/javascript"></script>
    <script src="" type="text/javascript"></script>
                    <script type="text/javascript">
                        

                        $('#playlist .item').hover(
                            function () {
                                $(this).children('.hidden-options').fadeIn(0);
                            },
                            function () {
                                $(this).children('.hidden-options').fadeOut(0);
                            }
                        );
                        mymusic = null;
                        prevItem = null;
                        function playMusic(item) {
                            $(item).prev('.item').css('border-bottom', '1px solid #000');
                            $('#playlist div.expanded').fadeOut(0);
                            $(item).children('div.expanded').fadeIn(0);
                            $(item).unbind('mouseenter mouseleave');
                            $(prevItem).bind({
                                mouseenter: function (e) {
                                    $(this).children('.hidden-options').fadeIn(0);
                                    //$(this).prev('.item').css('border-bottom', '1px solid #000');
                                },
                                mouseleave: function (e) {
                                    $(this).children('.hidden-options').fadeOut(0);
                                    //$(this).prev('.item').css('border-bottom', '1px dotted #000');
                                }
                            });
                            $(prevItem).removeClass('now-playing');

                            var url = $(item).children('div.hidden-options').children('.download').children('a').attr('href');
                            var duration = $(item).children('.duration').attr('value'); //alert(duration);
                            soundManager.stop('music');
                            soundManager.createSound({
                                id: 'music',
                                url: url,
                                usePeakData: false,
                                useWaveformData: false,
                                useEQData: true,
                                onstop: function () {
                                    this.destruct();
                                },
                                onfinish: function () {
                                    this.stop();
                                    playMusic($(item).next('.item'));
                                },
                                whileplaying: function () {
                                    console.log('spec value = ' + this.eqData);
                                }
                            });
                            soundManager.play('music');
                            //window.setTimeout(function () {
                              //  var thi = soundManager.getSoundById('music');
                                ////var spectrumValue = (+thi.eqData.left[1] + +thi.eqData.right[1]) / 2.0;
                                //console.log('spec value = ' + thi.eqData);
                            //}, 1000 / 60);
                            mymusic = $(item);

                            var singer = $(item).children('.title').children('.singer').html()
                            var song = $(item).children('.title').children('.song').html()
                            var title = singer + song;

                            if (title.length > 55) {
                                song = song.substring(0, song.length - (title.length - 55)) + '...';
                            }

                            $('#now-playing-music-title .singer').html(singer);
                            $('#now-playing-music-title .song').html(song);

                            $(item).addClass('now-playing');
                            //alert(url);
                            prevItem = item;
                        }
                        $('#controls .next-btn').click(function () {
                            mymusic.removeClass('now-playing');
                            mymusic = mymusic.next('.item');
                            playMusic(mymusic);
                            mymusic.addClass('now-playing');
                        });

                        $('#controls .prev-btn').click(function () {
                            mymusic.removeClass('now-playing');
                            mymusic = mymusic.prev('.item');
                            playMusic(mymusic);
                            mymusic.addClass('now-playing');
                        });

                        $('#controls .play-btn').click(function () {
                            if (!$(this).hasClass('paused')) {
                                soundManager.pause('music');
                                $(this).html('Play');
                                $(this).addClass('paused');
                            }
                            else {
                                soundManager.play('music');
                                $(this).html('Pause');
                                $(this).removeClass('paused');
                            }
                        });
                    </script>

</body>
</html>
<script type="text/javascript">
    //alert($(window).height());
</script>

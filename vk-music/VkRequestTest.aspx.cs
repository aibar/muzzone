﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using cloudIMusic;

namespace vk_music
{
    public partial class VkRequestTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            VKApi.VKApp vkApp;

            vkApp = VKApi.VKApp.Instance;
            vkApp.AppId = "3060820";
            vkApp.IsCrossDomain = true;
            vkApp.User = new VKApi.VKUser() { AccessToken = "6b66cd4561d5b97361d5b973ea61fb0d27661d561d4b97b317575fadf25de1c2fd2ae1d" };

            vkApp.Request = new VKApi.VKRequest.Request("audio.getById");
            vkApp.Request.Params = new List<KeyValuePair<string, string>>() {
                    new KeyValuePair<string, string>("v", "2.0"),
                    new KeyValuePair<string, string>("audios", "-4564345_86388949")
                };

            this.Result.Text = vkApp.DoRequest();
        }
    }
}

/*
 {"response":[{"aid":86388949,"owner_id":-4564345,"artist":"Metric","title":"Help I'm Alive-Дневники Вампира 2 серия","duration":286,"url":"http:\/\/cs4631.userapi.com\/u59628032\/audios\/20d3eaf2f7f1.mp3","lyrics_id":"3122791"}]}
*/
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cloudIMusic
{
    public class PlaylistMusic
    {
        public string MusicFullName { get; set; }
        public string URL { get; set; }
        public int Position { get; set; }
        public string Time { get; set; }
        public string MusicFullNameToShow
        {
            get 
            {
                if (this.MusicFullName.Length > 65)
                    return this.MusicFullName.Substring(0, 65) + "...";
                else
                    return this.MusicFullName;
            }
        }
    }
}
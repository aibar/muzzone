﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.IO;
using System.Text;

using cloudIMusic;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace VKApi
{
    public static class VK
    {
        public static string ApiBaseUrl = "https://api.vk.com/method";
        public static string AudioSearchUrl = "https://api.vk.com/method/audio.search";
    }

    public static class VKMethods
    {
        private static string access_token = "d9f966c2b81a441f0fc734b80f6155ef11498e9601b5f3246113fd92328de03028f45a8ea0f529d32ac92";
        private static string api_id = "3060820";
        private static string v = "2.0";

        public static List<PlaylistMusic> AudioGet (
            string q = "green day 21st century breakdown",
            string sort = "0",
            string lyrics = "0",
            string count = "10", 
            string offset = "0", 
            string test_mode = "1")
        {
            List<PlaylistMusic> playlist = new List<PlaylistMusic>(10);

            WebClient webClient = new WebClient();
            webClient.BaseAddress = "https://api.vkontakte.ru/";
            webClient.QueryString.Add("q", q);
            webClient.QueryString.Add("sort", sort);
            webClient.QueryString.Add("lyrics", lyrics);
            webClient.QueryString.Add("count", count);
            webClient.QueryString.Add("offset", offset);
            webClient.QueryString.Add("test_mode", test_mode);
            webClient.QueryString.Add("access_token", VKMethods.access_token);
            webClient.QueryString.Add("api_id", VKMethods.api_id);
            webClient.QueryString.Add("v", VKMethods.v);

            webClient.Encoding = Encoding.UTF8;

            //using (var client = new WebClient())
            //{
            //    var values = new NameValueCollection
            //    {
            //        { "param1", "value 1" },
            //        { "param2", "value 2" },
            //    };
            //    byte[] result = client.UploadValues("https://api.vkontakte.ru/audio.search", values);
            //    // TODO: process the results
            //}
            string json;
            try
            {
                json = webClient.DownloadString("https://api.vkontakte.ru/method/audio.search");
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                webClient.Dispose(); 
            }

            //var audioResp = JsonConvert.DeserializeObject<AudioGetResponseRoot>(json);
            var audioResp = AudioSearchResponse.FromJson(json);
            //var audioCount = ((Newtonsoft.Json.Linq.JObject)audioResp.Response[0]).;
            for (int i = 1; i < audioResp.Response.Length; i++)
            {
                var audio = JsonConvert.DeserializeObject<VKAudio>(((Newtonsoft.Json.Linq.JContainer)audioResp.Response[i]).ToString());
                playlist.Add(new PlaylistMusic()
                {
                    MusicFullName = audio.url,//audio.artist + " - " + audio.title,
                    Position = i,
                    URL = audio.url,
                    Time = audio.duration + "0"
                });
            }

            return playlist;
        }

        public static List<PlaylistMusic> AudioGet(string json)
        {
            List<PlaylistMusic> playlist = new List<PlaylistMusic>(10);

            var audioResp = AudioSearchResponse.FromJson(json); //var audioCount = ((Newtonsoft.Json.Linq.JObject)audioResp.Response[0]).;
            for (int i = 1; i < audioResp.Response.Length; i++)
            {
                var audio = JsonConvert.DeserializeObject<VKAudio>(((Newtonsoft.Json.Linq.JContainer)audioResp.Response[i]).ToString());
                playlist.Add(new PlaylistMusic()
                {
                    MusicFullName = audio.artist + " - " + audio.title,
                    Position = i,
                    URL = audio.url,
                    Time = audio.duration + "0"
                });
            }

            return playlist;
        }

        public static string GetJson (
            string q = "30 seconds to mars",
            string sort = "0",
            string lyrics = "0",
            string count = "10",
            string offset = "0",
            string test_mode = "1")
        {
            WebClient webClient = new WebClient();
            webClient.BaseAddress = "https://api.vkontakte.ru/";
            webClient.QueryString.Add("q", q);
            webClient.QueryString.Add("sort", sort);
            webClient.QueryString.Add("lyrics", lyrics);
            webClient.QueryString.Add("count", count);
            webClient.QueryString.Add("offset", offset);
            webClient.QueryString.Add("test_mode", test_mode);
            webClient.QueryString.Add("access_token", VKMethods.access_token);
            webClient.QueryString.Add("api_id", VKMethods.api_id);
            webClient.QueryString.Add("v", VKMethods.v);
            webClient.QueryString.Add("format", "XML");

            webClient.Encoding = Encoding.UTF8;

            //using (var client = new WebClient())
            //{
            //    var values = new NameValueCollection
            //    {
            //        { "param1", "value 1" },
            //        { "param2", "value 2" },
            //    };
            //    byte[] result = client.UploadValues("https://api.vkontakte.ru/audio.search", values);
            //    // TODO: process the results
            //}
            string json;
            byte[] jsonBytes;
            try
            {
                json = webClient.DownloadString("https://api.vkontakte.ru/method/audio.search.xml");
                jsonBytes = webClient.DownloadData("https://api.vkontakte.ru/method/audio.search");
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                webClient.Dispose();
            }

            return json;
        }
    }
}
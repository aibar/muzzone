﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.IO;
using System.Text;

using Newtonsoft.Json;

namespace VKApi
{
    [Serializable]
    public class AudioSearchResponse
    {

        public object[] Response;

        //Empty Constructor
        public AudioSearchResponse() { }

        public string Serialize()
        {
            return JsonConvert.SerializeObject(this);
        }
        public static AudioSearchResponse FromJson(string json)
        {
            return JsonConvert.DeserializeObject<AudioSearchResponse>(json);
        }
    }

    public class VKAudio
    {
        public string aid { get; set; }
        public string owner_id { get; set; }
        public string artist { get; set; }
        public string title { get; set; }
        public string duration { get; set; }
        public string url
        {
            get { return this._url; }
            set { this._url = value.Replace('\\', '/'); }
        }
        public string lyrics_id { get; set; }

        private string _url;
    }

    public class VKUser
    {
        public string UserId { get; set; }
        public string AccessToken { get; set; }
    }

    // Singleton
    public class VKApp
    {
        private VKApp()
        { }

        public static VKApp Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VKApp();
                }
                return instance;
            }
        }
        public string AppId { get; set; }
        public VKUser User { get; set; }
        public VKRequest.Request Request { get; set; }
        public bool IsCrossDomain { get; set; }

        private static VKApp instance;

        public Uri GetRequestUri()
        {
            string reqUri = this.Request.BaseAddress + "/" + this.Request.Method + "?";
            foreach (KeyValuePair<string, string> key in this.Request.Params)
            {
                reqUri += (key.Key + "=" + key.Value + "&");
            }

            reqUri += ("app_id=" + this.AppId + "&access_token=" + this.User.AccessToken);

            if (this.IsCrossDomain)
                reqUri += ("&callback=" + this.Request.CallbackFunc);

            return new Uri(reqUri);
        }

        public string DoRequest()
        {
            WebClient webClient = new WebClient();
            webClient.BaseAddress = this.Request.BaseAddress;
            foreach (KeyValuePair<string, string> key in this.Request.Params)
            {
                webClient.QueryString.Add(key.Key, key.Value);
            }           

            webClient.Encoding = Encoding.UTF8;

            string json;
            try
            {
                json = webClient.DownloadString(this.GetRequestUri());
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                webClient.Dispose();
            }

            return json;
        }
    }
}
//{"aid":86388949,"owner_id":-4564345,"artist":"Metric","title":"Help I'm Alive","duration":286,"
//url":"http:\/\/cs4631.userapi.com\/u59628032\/audios\/f95c3abaf6d0.mp3","lyrics_id":"3122791"},
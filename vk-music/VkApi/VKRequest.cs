﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VKApi.VKRequest
{
    public class Request
    {
        public Request(string method, bool isCrossDomain = false, string callbackFunc = null)
        {
            this.baseAddress = "https://api.vkontakte.ru/method";
            this.Method = method;
            this.IsCrossDomain = isCrossDomain;
            this.CallbackFunc = callbackFunc;
        }

        public string BaseAddress 
        {
            get { return this.baseAddress; }
            private set { this.baseAddress = value; }
        }
        public bool IsCrossDomain { get; set; }
        public string Method { get; set; }
        public string CallbackFunc { get; set; }
        public List<KeyValuePair<string, string>> Params { get; set; }

        private string baseAddress;
    }
}